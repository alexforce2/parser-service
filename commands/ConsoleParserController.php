<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\admin\models\Parser;
use app\modules\admin\models\Proxy;
use app\components\ConsoleRunner;

class ConsoleParserController extends Controller
{
    public function actionStart($id)
    {
        if (!empty($id)) {
            $parser = Parser::find()->with('proxy')->where(['id' => $id])->one();
            $startParserCondition = $parser && $parser->working && $parser->proxy && $parser->proxy->valid && $parser->proxy->working;

            if ($startParserCondition) {
                $url = 'https://export.yandex.ru/last/last20x.xml';

                try {
                    $content = $parser->proxy->getContentViaProxy($parser->proxy_id, $url);

                    if (!empty($content)) {
                        //disabling XML errors
                        libxml_use_internal_errors(true);
                        $xml = new \SimpleXMLElement($content);
                        /**/
                        if (!empty($xml->last20x->item)) {
                            $date = time();
                            $insertData = [];
                            foreach ($xml->last20x->item as $item) {
                                $insertData[] = [(string)$item, (int)$item['found'], $date];
                            }

                            $connection = Yii::$app->db;
                            $sqlCommand = $connection->createCommand()->batchInsert('parser_found_query', ['query', 'found', 'date'], $insertData);
                            $sql = $sqlCommand->getSql() . ' ON DUPLICATE KEY UPDATE `query`=`query`;';
                            $insertResult = $connection->createCommand($sql)->execute();
                            $parser->parsed_at = time();
                            $parser->inserted_rows = (int)$insertResult;
                            $parser->save();
                        }

                        if ($startParserCondition) {
                            $delay = $parser->interval ? (int)$parser->interval : 15;
                            sleep($delay);
                            $console = new ConsoleRunner(['file' => '@app/yii']);
                            $console->run("console-parser/start {$parser->id}");
                        }
                    }
                } catch (\Exception $e) {

                }
            }

        }
    }
} 