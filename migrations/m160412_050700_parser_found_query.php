<?php

use yii\db\Migration;

class m160412_050700_parser_found_query extends Migration
{
    private $tableName = 'parser_found_query';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'query' => $this->string(255)->notNull(),
            'found' => $this->integer()->notNull(),
            'date' => $this->integer()->notNull(),
        ]);
        $this->createIndex('unique_query', $this->tableName, 'query', true);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
