<?php

use yii\db\Migration;

class m160414_102226_parser_add_columns_parsed_at_and_inserted_rows extends Migration
{
    private $tableName = 'parser';

    public function up()
    {
        $this->addColumn($this->tableName, 'parsed_at', 'INTEGER');
        $this->addColumn($this->tableName, 'inserted_rows', 'INTEGER');
    }

    public function down()
    {
        $this->dropColumn($this->tableName, 'parsed_at');
        $this->dropColumn($this->tableName, 'inserted_rows');
    }
}
