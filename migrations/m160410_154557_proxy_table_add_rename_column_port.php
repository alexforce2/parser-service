<?php

use yii\db\Migration;

class m160410_154557_proxy_table_add_rename_column_port extends Migration
{
    public function up()
    {
        $this->renameColumn('proxy', 'port', 'port_http');
    }

    public function down()
    {
        echo "m160410_154557_proxy_table_add_rename_column_port cannot be reverted.\n";
        return false;
    }
}
