<?php

use yii\db\Migration;

class m160412_050552_new_parser_table extends Migration
{
    private $tableName = 'parser';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'proxy_id' => $this->integer()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'interval' => $this->integer()->defaultValue(15)->notNull(),
            'working' => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);
        $this->createIndex('unique_name', $this->tableName, 'name', true);
        $this->addForeignKey('proxy_id_ref', $this->tableName, 'proxy_id', 'proxy', 'id', 'SET NULL');
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
