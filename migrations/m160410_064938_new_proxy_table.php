<?php

use yii\db\Migration;
use yii\db\Schema;

class m160410_064938_new_proxy_table extends Migration
{
    private $tableName = 'proxy';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'ip' => $this->string(15)->notNull(),
            'port' => $this->integer()->notNull(),
            'login' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'valid' => $this->smallInteger()->defaultValue(0)->notNull(),
            'city' => $this->string(),
            'working' => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);
        $this->createIndex('unique_ip', $this->tableName, 'ip', true);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
