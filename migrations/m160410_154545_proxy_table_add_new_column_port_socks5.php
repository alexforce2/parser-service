<?php

use yii\db\Migration;

class m160410_154545_proxy_table_add_new_column_port_socks5 extends Migration
{
    private $tableName = 'proxy';
    private $columnName = 'port_socks5';

    public function up()
    {
        $this->addColumn($this->tableName, $this->columnName, 'integer(11)');
    }

    public function down()
    {
        $this->dropColumn($this->tableName, $this->columnName);
        return true;
    }
}
