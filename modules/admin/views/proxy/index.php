<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProxySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proxies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proxy-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Proxy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'ip',
            'city',
            [
                'attribute' => 'working',
                'content' => function($data)
                {
                    return $data->working ? 'Включён' : 'Отключён';
                }
            ],
            [
                'attribute' => 'valid',
                'content' => function($data)
                {
                    return $data->valid ? 'Валидный' : 'Не валидный';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {link}',
                'buttons' => [
                    'link' => function ($url, $model, $key) {
                        return Html::a('Check Proxy', '/admin/proxy/check-proxy-connect/' . $key);
                    },
                ],

            ],
        ],
    ]);
    Pjax::end();
?>
</div>
