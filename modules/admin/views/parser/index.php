<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\modules\admin\models\Parser */
/* @var $queryCount integer */

$this->title = 'Parsers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-parser-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Всего запросов в базе: <?= number_format($queryCount); ?>
    </p>

    <p>
       Дата сервера: <?= date('d.m.Y H:i:s', time()); ?>
    </p>

    <p>
        <?= Html::a('Create Parser', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'proxy.ip',
            'name',
            'interval',
            [
                'attribute' => 'working',
                'content' => function($data)
                {
                    return $data->working ? 'Включён' : 'Отключён';
                }
            ],
            [
                'attribute' => 'parsed_at',
                'content' => function($data)
                {
                    return $data->parsed_at ? date('d.m.Y H:i:s', $data->parsed_at) : 'Не запускался';
                }
            ],
            'inserted_rows',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {link}',
                'buttons' => [
                    'link' => function ($url, $model, $key) {
                        $statusName = $model->working ? 'Отключить' : 'Включить';
                        return Html::a($statusName, '/admin/parser/switch/' . $key );
                    },
                ],

            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
