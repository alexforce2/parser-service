<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Proxy;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Parser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parser-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $ip = Proxy::find()->all();
    $items = ArrayHelper::map($ip, 'id', 'ip');
    $params = ['prompt' => 'Выберите IP'];
    echo $form->field($model, 'proxy_id')->dropDownList($items, $params);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'interval')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
