<?php

namespace app\modules\admin\models;

use Yii;
use app\components\ConsoleRunner;

/**
 * This is the model class for table "parser".
 *
 * @property integer $id
 * @property integer $proxy_id
 * @property string $name
 * @property integer $interval
 * @property integer $working
 * @property integer $parsed_at
 * @property integer $inserted_rows
 *
 * @property Proxy $proxy
 */
class Parser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proxy_id', 'interval', 'working'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['proxy_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proxy::className(), 'targetAttribute' => ['proxy_id' => 'id']],
            ['interval', 'number', 'min' => 5, 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proxy_id' => 'Proxy ID',
            'name' => 'Name',
            'interval' => 'Interval',
            'working' => 'Working',
            'parsed_at' => 'Last parsed date',
            'inserted_rows' => 'Last inserted rows',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return $this->hasOne(Proxy::className(), ['id' => 'proxy_id']);
    }

    public function switchParser($id)
    {
        $resultArray = [
            'status' => 'danger',
            'message' => 'Ошибка',
        ];

        $parser = $this->findOne($id);

        if ($parser) {
            $parser->working = $parser->working ? 0 : 1;
            if ($parser->save()) {
                $parserEnabledMessage = "Парсер \"{$parser->name}\" включён, интервал запросов {$parser->interval} сек.";
                $parserDisabledMessage = "Парсер \"{$parser->name}\" отключён.";
                $resultArray['message'] = $parser->working ? $parserEnabledMessage : $parserDisabledMessage;
                $resultArray['status'] = 'success';

                if ($parser->working) {
                    $this->startParser($parser->id);
                }
            } else {
                $resultArray['message'] = 'Ошибка при записи в БД!';
            }
        } else {
            $resultArray['message'] = "Парсер с ID {$id} не найден!";
        }

        return $resultArray;
    }

    private function startParser($id)
    {
        $parser = (empty($this->id)) ? Parser::find()->with('proxy')->where(['id' => $id])->one() : $this;
        if ($parser && $parser->working && $parser->proxy) {
            $console = new ConsoleRunner(['file' => '@app/yii']);
            $console->run("console-parser/start {$parser->id}");
        }
        return true;
    }
}
