<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "proxy".
 *
 * @property integer $id
 * @property string $ip
 * @property integer $port_http
 * @property integer $port_socks5
 * @property string $login
 * @property string $password
 * @property integer $valid
 * @property string $city
 * @property integer $working
 */
class Proxy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['ip', 'ip'],
            [['ip', 'port_http', 'port_socks5', 'login', 'password'], 'required'],
            [['port_http', 'port_socks5', 'valid', 'working'], 'integer'],
            [['ip'], 'string', 'max' => 15],
            [['login', 'password', 'city'], 'string', 'max' => 255],
            [['ip'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
            'port_http' => 'Port HTTP',
            'port_socks5' => 'Port SOCKS5',
            'login' => 'Login',
            'password' => 'Password',
            'valid' => 'Valid',
            'city' => 'City',
            'working' => 'Working',
        ];
    }

    public function checkProxyConnect($proxyId)
    {
        $resultArray = [
            'result' => false,
            'message' => '',
        ];

        if (!empty($proxyId) && $proxy = $this->findOne($proxyId)) {
            $url = 'https://tune.yandex.ru/region/';
            $city = 'Ошибка';
            $content = $this->getContentViaProxy($proxyId, $url);

            if ($content) {
                $resultArray['message'][] = 'Соединение установлено, IP: ' . $proxy->ip . '.';
                //disabling XML errors
                libxml_use_internal_errors(true);
                $dom = new \DOMDocument();
                $dom->loadHTML($content);
                $dom->validateOnParse = true;

                $xPath = new \DOMXPath($dom);
                //xpath query для парсинга города с главной страницы яндекса
//                $elements = $xPath->query('body/div[1]/div[1]/div/div[1]/div/div[1]');
                //xpath query для парсинга города со страницы https://tune.yandex.ru/region/
                $elements = $xPath->query('body/table[2]/tr/td[3]/form/span[1]/span[1]/input/@value');

                if (!empty($elements) && $elements->length === 1) {
                    foreach ($elements as $e) {
                        $city = $e->nodeValue;
                    }

                    $proxy->city = $city;
                    $proxy->valid = 1;
                    $proxy->working = 1;
                    $resultArray['result'] = true;
                    $resultArray['message'][] = 'Город: ' . $proxy->city . '.';

                } else {
                    $resultArray['message'][] = HtmlPurifier::process(preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $content));
                    $proxy->city = $city;
                    $proxy->valid = 0;
                    $proxy->working = 0;
                    $resultArray['result'] = false;
                    $resultArray['message'][] = 'Ошибка при парсинге города, проверьте XPath.';
                }
            } else {
                $proxy->city = $city;
                $proxy->valid = 0;
                $proxy->working = 0;

                $resultArray['message'][] = 'Ошибка при соединении!';
            }
        } else {
            $resultArray['message'][] = 'Proxy с ID' . $proxyId . ' не найден!';
        }

        if (!empty($proxy) && !$proxy->save()) {
            $resultArray['message'][] = 'Ошибка при записи в БД!';
        }

        return $resultArray;
    }

    public function getContentViaProxy($proxyId, $url)
    {
        $proxy = $this->findOne($proxyId);
        $content = null;
        if ($proxy) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy->ip . ':' . $proxy->port_http);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy->login . ':' . $proxy->password);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($ch);
            curl_close($ch);
        }
        return $content;
    }
}
