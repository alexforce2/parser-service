<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "parser_found_query".
 *
 * @property integer $id
 * @property string $query
 * @property integer $found
 * @property integer $date
 */
class ParserFoundQuery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parser_found_query';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query', 'found', 'date'], 'required'],
            [['found', 'date'], 'integer'],
            [['query'], 'string', 'max' => 255],
            [['query'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'query' => 'Query',
            'found' => 'Found',
            'date' => 'Date',
        ];
    }

    public static function getRowsCount()
    {
        $sql = 'SELECT COUNT(*) as `count` FROM `' . self::tableName() . '`';
        $queryCount = Yii::$app->db->createCommand($sql)->queryAll();

        if (!empty($queryCount[0]['count'])) {
            $queryCount = $queryCount[0]['count'];
        }

        return (int)$queryCount;
    }
}
